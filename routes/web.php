<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\SessionController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\DataMasterLayanan;
use App\Http\Controllers\UserController;
use App\Http\Controllers\TutorialController;
use App\Http\Controllers\DosirDigitalController;
use App\Http\Controllers\RemindersController;

Route::get('/', [SessionController::class, 'signin'])->name('login');
Route::get('/logout', [SessionController::class, 'logout'])->name('logout');
Route::get('/signup', [SessionController::class, 'signup'])->name('signup');

// Route::group(['middleware' => 'auth'], function () {
// DASHBOARD
Route::get('/dashboard', [DashboardController::class, 'dashboard'])->name('dashboard');

// LOGIN
Route::post('/proses-loginn', [SessionController::class, 'loginProses'])->name('signin.proses');

// LAYANAN APLIKASI
Route::get('/data-master-kategori', [DataMasterLayanan::class, 'layanan'])->name('layanan');
Route::get('/data-master-kategori/list', [DataMasterLayanan::class, 'layanan_list'])->name('layanan_list');
Route::get('/data-master-kategori-tambah', [DataMasterLayanan::class, 'layanan_tambah'])->name('layanan_tambah');
Route::get('/data-master-kategori-ubah/{id}', [DataMasterLayanan::class, 'layanan_edit'])->name('layanan_ubah');
Route::post('/data-master-kategori-update', [DataMasterLayanan::class, 'layanan_update'])->name('layanan_update');
Route::post('/data-master-kategori-simpan', [DataMasterLayanan::class, 'layanan_simpan'])->name('layanan_simpan');
Route::get('/data-master-kategori-lihat/{id}', [DataMasterLayanan::class, 'layanan_lihat'])->name('layanan_lihat');
Route::get('/data-master-kategori-hapus/{id}', [DataMasterLayanan::class, 'layanan_hapus'])->name('layanan_hapus');

// Route::get('/data-layanan-aplikasi/{id}', [DataMasterLayanan::class, 'reminder_layanan'])->name('layanan_aplikasi');
Route::get('/data-master-aplikasi/{id}', [DataMasterLayanan::class, 'aplikasi'])->name('aplikasi');
Route::get('/data-master-aplikasi/list', [DataMasterLayanan::class, 'aplikasi_list'])->name('aplikasi_list');
Route::get('/data-master-aplikasi-tambah/{id}', [DataMasterLayanan::class, 'aplikasi_tambah'])->name('aplikasi_tambah');
Route::get('/data-master-aplikasi-ubah/{id}', [DataMasterLayanan::class, 'aplikasi_edit'])->name('aplikasi_ubah');
Route::post('/data-master-aplikasi-update', [DataMasterLayanan::class, 'aplikasi_update'])->name('aplikasi_update');
Route::post('/data-master-aplikasi-simpan', [DataMasterLayanan::class, 'aplikasi_simpan'])->name('aplikasi_simpan');
Route::get('/data-master-aplikasi-lihat/{id}', [DataMasterLayanan::class, 'aplikasi_lihat'])->name('aplikasi_lihat');
Route::get('/data-master-aplikasi-hapus/{id}', [DataMasterLayanan::class, 'aplikasi_hapus'])->name('aplikasi_hapus');



// TUTORIAL
Route::get('/data-master-tutorial', [TutorialController::class, 'tutorial'])->name('tutorial');
Route::get('/data-master-tutorial/list', [TutorialController::class, 'tutorial_list'])->name('tutorial_list');
Route::get('/data-master-tutorial-tambah', [TutorialController::class, 'tutorial_tambah'])->name('tutorial_tambah');
Route::get('/data-master-tutorial-ubah/{id}', [TutorialController::class, 'tutorial_ubah'])->name('tutorial_ubah');
Route::post('/data-master-tutorial-simpan', [TutorialController::class, 'tutorial_simpan'])->name('tutorial_simpan');
Route::post('/data-master-tutorial-update', [TutorialController::class, 'tutorial_update'])->name('tutorial_update');
Route::get('/data-master-tutorial-lihat/{id}', [TutorialController::class, 'tutorial_lihat'])->name('tutorial_lihat');
Route::get('/data-master-tutorial-hapus/{id}', [TutorialController::class, 'tutorial_hapus'])->name('tutorial_hapus');

// USER MANAGEMENT
Route::get('/data-master-users', [UserController::class, 'users'])->name('users');
Route::get('/data-master-users/list', [UserController::class, 'users_list'])->name('users_list');
Route::get('/data-master-users-tambah', [UserController::class, 'users_tambah'])->name('users_tambah');
Route::get('/data-master-users-ubah', [UserController::class, 'users_ubah'])->name('users_ubah');
Route::post('/data-master-users-simpan', [UserController::class, 'users_simpan'])->name('users_simpan');
Route::get('/data-master-users-lihat/{id}', [UserController::class, 'users_lihat'])->name('users_lihat');
Route::get('/data-master-users-edit/{id}', [UserController::class, 'users_edit'])->name('users_edit');
Route::get('/data-master-users-hapus/{id}', [UserController::class, 'users_delete'])->name('users_delete');

// REMINDERS
// LAYANAN APLIKASI
Route::get('/data-reminder-layanan/{id}', [RemindersController::class, 'reminder_layanan'])->name('reminder_aplikasi');
Route::get('/data-reminder', [RemindersController::class, 'reminder'])->name('reminder');
Route::get('/data-reminder/list', [RemindersController::class, 'reminder_list'])->name('reminder_list');
Route::get('/data-reminder-tambah/{id}', [RemindersController::class, 'reminder_tambah'])->name('reminder_tambah');
Route::get('/data-reminder-ubah/{id}', [RemindersController::class, 'reminder_edit'])->name('reminder_ubah');
Route::post('/data-reminder-update', [RemindersController::class, 'reminder_update'])->name('reminder_update');
Route::post('/data-reminder-simpan', [RemindersController::class, 'reminder_simpan'])->name('reminder_simpan');
Route::post('/data-reminder-dosir', [RemindersController::class, 'reminder_dosir'])->name('reminder_dosir');
Route::get('/data-reminder-lihat/{id}', [RemindersController::class, 'reminder_lihat'])->name('reminder_lihat');
Route::get('/data-reminder-hapus/{id}', [RemindersController::class, 'reminder_hapus'])->name('reminder_hapus');

// DOSIR DIGITAL
Route::get('/data-dosir-digital', [DosirDigitalController::class, 'dosirdigital'])->name('dosirdigital');
Route::get('/data-dosir-digital-buat', [DosirDigitalController::class, 'addDosirDigital'])->name('dosirdigital.add');
Route::post('/data-dosir-digital-save', [DosirDigitalController::class, 'reminder_dosir'])->name('dosirdigital.save');
Route::get('/data-dosir-digital-lihat/{id}', [DosirDigitalController::class, 'lihatDosir'])->name('dosirdigital.view');
Route::get('/data-dosir-digital-hapus/{id}', [DosirDigitalController::class, 'hapusDosir'])->name('dosirdigital.delete');
// });




// Route::group(['middleware' => 'auth','role:4'], function () {
    // Route::prefix('pemohon')->group(function () {

        // PERMOHONAN


        // });
