<div class="sidebar-wrapper sidebar-theme">

<header class="main-nav">
    <div class="sidebar-user text-center">
        <img class="img-90" src="{{asset('assets/logos.png')}}" alt="">
        <div class="badge-bottom"><span class="badge badge-primary">New</span></div><a href="user-profile.html">
        <h6 class="mt-3 f-14 f-w-600">{{Auth()->user()->nama}}</h6></a>
        {{-- <p class="mb-0 font-roboto">Admin / Pegawai</p> --}}
    </div>
    <nav>
        <div class="main-navbar">
            <div class="left-arrow" id="left-arrow"><i data-feather="arrow-left"></i></div>
                <div id="mainnav">
                    <ul class="nav-menu custom-scrollbar">
                    <li class="back-btn">
                        <div class="mobile-back text-end"><span>Back</span><i class="fa fa-angle-right ps-2" aria-hidden="true"></i></div>
                    </li>
                    <li class="dropdown {{ request()->is('dashboard*') ? 'active' : '' }}"><a class="nav-link menu-title link-nav" href="{{ route('dashboard') }}"><i data-feather="home"></i><span>Dashboard</span></a></li>
                    @if (Auth()->user()->role == 1)
                    <li class="dropdown"><a class="nav-link menu-title link-nav" href="{{route('layanan')}}"><i data-feather="layout"></i><span>Data Master Kategori</span></a></li>
                    <li class="dropdown"><a class="nav-link menu-title link-nav" href="{{ route('tutorial') }}"><i data-feather="file"></i><span>Tutorial</span></a></li>
                    <li class="dropdown"><a class="nav-link menu-title link-nav" href="{{ route('users') }}"><i data-feather="users"></i><span>User Management</span></a></li>
                    <li class="dropdown"><a class="nav-link menu-title link-nav" href="{{ route('reminder') }}"><i data-feather="alert-triangle"></i><span>Reminders</span></a></li>
                    @else
                    <li class="dropdown"><a class="nav-link menu-title link-nav" href="{{ route('reminder') }}"><i data-feather="alert-triangle"></i><span>Reminders</span></a></li>
                    <li class="dropdown"><a class="nav-link menu-title link-nav" href="{{ route('dosirdigital') }}"><i data-feather="alert-triangle"></i><span>Dosir Digital</span></a></li>
                    @endif
             </div>
            <div class="right-arrow" id="right-arrow"><i data-feather="arrow-right"></i></div>
        </div>
    </nav>
</header>
