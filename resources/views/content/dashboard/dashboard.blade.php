@extends('layouts.master')
@section('judul', 'SAELAKONA - Dashboard')
@section('page-css')

@endsection
@section('content')

<div class="page-body">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-sm-6 mt-5">
                    <h3>Dashboard</h3>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6">
                <div class="card o-hidden border-0">
                    <div class="bg-primary b-r-4 card-body">
                        <div class="media static-top-widget">
                            <div class="align-self-center text-center"><i data-feather="file"></i></div>
                            <div class="media-body"><span class="m-0">Total Pegawai</span>
                                <h4 class="mb-0 counter">{{$pegawai}}</h4><i class="icon-bg" data-feather="file"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="card o-hidden border-0">
                    <div class="bg-secondary b-r-4 card-body">
                        <div class="media static-top-widget">
                            <div class="align-self-center text-center"><i data-feather="file"></i></div>
                            <div class="media-body"><span class="m-0">Total Data Laporan</span>
                                <h4 class="mb-0 counter">{{$reminder}}</h4><i class="icon-bg" data-feather="file"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Data Laporan Aplikasi</h5>
                    </div>
                    <div class="bar-chart-widget">
                        <div class="bottom-content card-body">
                            <div class="row">
                                <div class="col-12">
                                    <div id="chart-widget4"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Data Laporan Aplikasi</h5>
                    </div>
                    <div class="card-body">
                        <div class="table">
                            <table class="display" id="basic-1">
                                <thead>
                                    <tr>
                                        <th>Nama Pegawai</th>
                                        <th>NIP</th>
                                        <th>Aplikasi</th>
                                        <th>Tanggal</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ( $reminder_all as $rows)
                                    <tr>
                                        <td>{{$rows->pegawai->nama}}</td>
                                        <td>{{$rows->pegawai->nip}}</td>
                                        <td>{{$rows->kategori->nama_layanan}}</td>
                                        <td>{{$rows->waktu_pengisian}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('page-js')

@endsection
