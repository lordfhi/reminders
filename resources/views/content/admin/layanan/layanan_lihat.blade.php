@extends('layouts.master')
@section('judul', 'Lihat Kategori')
@section('page-css')

@endsection
@section('content')

<div class="page-body">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-sm-6">
                    <h3>{{ $judul }}</h3>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                        <li class="breadcrumb-item">Kategori</li>
                        <li class="breadcrumb-item active">Formulir Kategori</li>
                    </ol>
                </div>
                <div class="col-lg-6 text-end">
                    {{-- <button readonly type="button" class="btn btn-primary btn-sm">Buat Permohonan</button>  --}}
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <form class="theme-form">
                            <div class="row mb-3">
                                <div class="col-xl-12">
                                    <div class="mb-3">
                                        {{ Form::label('layanan', 'Kategori') }}
                                        {{ Form::text('layanan', $layanan->nama_layanan , ['class'=>'form-control', 'placeholder'=>'Masukan Nama Kategori','readonly']) }}
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-xl-12">
                                    <div class="mb-3">
                                        {{ Form::label('keterangan', 'Keterangan') }}
                                        <div class="card">
                                            <div class="card-body">
                                                <h1>{!! $layanan->keterangan !!}</h1>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <hr>

                            <div class="row mt-3">
                                <div class="col-lg-6">
                                    <a href="{{ url()->previous() }}" class="btn btn-warning btn-sm">Kembali</a>
                                </div>
                                <div class="col-lg-6 text-end">
                                    {{-- <a href="{{ route('permohonan') }}" class="btn btn-primary btn-sm">Simpan</a>  --}}
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
</div>

@endsection
@section('page-js')

<script>
    $(document).ready(function() {
        $('.select2').select2();
    });
</script>

@endsection
