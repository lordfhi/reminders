@extends('layouts.master')
@section('judul', 'Edit Aplikasi')
@section('page-css')

@endsection
@section('content')

<div class="page-body">
    <div class="container-fluid mt-3">
        <div class="page-header">
            <div class="row">
                <div class="col-sm-6">
                    <h3>{{ $judul }}</h3>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                        <li class="breadcrumb-item">Nama Aplikasi</li>
                        <li class="breadcrumb-item active">Formulir Nama Aplikasi</li>
                    </ol>
                </div>
                <div class="col-lg-6 text-end">

                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <form class="theme-form" method="POST" action="{{route('aplikasi_update')}}">
                            @csrf
                            <div class="row mb-3">
                                <div class="col-xl-12">
                                    <div class="mb-3">
                                        {{ Form::label('layanan', 'Nama Aplikasi') }}
                                        {{ Form::text('aplikasi', $aplikasi->nama_aplikasi , ['class'=>'form-control', 'placeholder'=>'Masukan Nama Kategori']) }}
                                        {{ Form::hidden('id', $aplikasi->id , ['class'=>'form-control', 'placeholder'=>'Masukan Nama Kategori','readonly']) }}
                                        {{ Form::hidden('id_layanan', $aplikasi->id_layanan , ['class'=>'form-control', 'placeholder'=>'Masukan Nama Kategori','readonly']) }}
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row mt-3">
                                <div class="col-lg-6">
                                    <a href="{{ url()->previous() }}" class="btn btn-warning btn-sm">Kembali</a>
                                </div>
                                <div class="col-lg-6 text-end">

                                    <button type="submit" class="btn btn-primary btn-sm">Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
</div>

@endsection
@section('page-js')

<script>
    $(document).ready(function() {
        $('.select2').select2();
    });
</script>

@endsection
