@extends('layouts.master')
@section('judul', 'Aplikasi')
@section('page-css')

@endsection
@section('content')

<div class="page-body">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-sm-6 mt-5">
                    <h3>{{ $judul }}</h3>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                        <li class="breadcrumb-item">Aplikasi</li>
                        <li class="breadcrumb-item active">Formulir Aplikasi</li>
                    </ol>
                </div>
                <div class="col-lg-6 text-end">
                    {{-- <button type="button" class="btn btn-primary btn-sm">Buat Permohonan</button>  --}}
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        {!! Form::open(['route' => ['aplikasi_simpan'],'files'=>true , 'class'=>'theme-form']) !!}
                            @csrf

                            @if($errors->any())
                            <div class="alert alert-danger">
                                &nbsp; Validation Failed : <br/>
                                    <ul class="p-l-20">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ ucfirst($error) }}</li>
                                    @endforeach
                                    </ul>
                            </div>
                            @endif
                            {{ Form::hidden('id', $id) }}
                            <div class="row mb-3">
                                <div class="col-xl-12">
                                    <div class="mb-3">
                                        {{ Form::label('layanan', 'Aplikasi') }}
                                        {{ Form::text('aplikasi', '' , ['class'=>'form-control', 'placeholder'=>'Masukan Nama Aplikasi']) }}
                                    </div>
                                </div>
                            </div>
                            <hr>

                            <div class="row mt-3">
                                <div class="col-xl-6">
                                    <a href="{{ url()->previous() }}" class="btn btn-warning btn-sm">Kembali</a>
                                </div>
                                <div class="col-xl-6 text-end">
                                    <button type="submit" class="btn btn-primary btn-sm">Simpan</button>
                                    {{-- <a href="{{ route('layanan') }}" class="btn btn-primary btn-sm">Simpan</a> --}}
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
    <!-- Container-fluid Ends-->
</div>

@endsection
@section('page-js')

<script>
    $(document).ready(function() {
        $('.select2').select2();
    });
</script>
<script src="{{asset('')}}assets/js/editor/ckeditor/ckeditor.js"></script>
<script src="{{asset('')}}assets/js/editor/ckeditor/adapters/jquery.js"></script>
<script src="{{asset('')}}assets/js/editor/ckeditor/styles.js"></script>
<script src="{{asset('')}}assets/js/editor/ckeditor/ckeditor.custom.js"></script>
@endsection
