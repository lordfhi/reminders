@extends('layouts.master')
@section('judul', 'Users')
@section('page-css')

@endsection
@section('content')

<div class="page-body">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-sm-6">
                    <h3>{{ $judul }}</h3>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                        <li class="breadcrumb-item">Users</li>
                        <li class="breadcrumb-item active">Formulir Users</li>
                    </ol>
                </div>
                <div class="col-lg-6 text-end">
                    {{-- <button type="button" class="btn btn-primary btn-sm">Buat Permohonan</button>  --}}
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        {!! Form::open(['route' => ['users_simpan'],'files'=>true , 'class'=>'theme-form']) !!}
                            @csrf

                            @if($errors->any())
                            <div class="alert alert-danger">
                                &nbsp; Validation Failed : <br/>
                                    <ul class="p-l-20">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ ucfirst($error) }}</li>
                                    @endforeach
                                    </ul>
                            </div>
                            @endif
                            {{ Form::hidden('id', $users->id) }}
                            <div class="row mb-3">
                                <div class="col-xl-12">
                                    <div class="mb-3">
                                        {{ Form::label('layanan', 'Nama Pegawai') }}
                                        {{ Form::text('nama_pegawai' , $users->nama, ['class'=>'form-control', 'placeholder'=>'Masukan Nama Pegawai']) }}
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-xl-12">
                                    <div class="mb-3">
                                        {{ Form::label('layanan', 'NIP') }}
                                        {{ Form::text('nip' , $users->nip, ['class'=>'form-control', 'placeholder'=>'Masukan NIP Pegawai']) }}
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-xl-12">
                                    <div class="mb-3">
                                        {{ Form::label('layanan', 'Email') }}
                                        {{ Form::text('email' , $users->email, ['class'=>'form-control', 'placeholder'=>'Masukan Email Pegawai']) }}
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-xl-12">
                                    <div class="mb-3">
                                        {{ Form::label('layanan', 'Password') }}
                                        {{ Form::text('password' , '', ['class'=>'form-control', 'placeholder'=>'Masukan Password Pegawai']) }}
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-xl-12">
                                    <div class="mb-3">
                                        {{ Form::label('layanan', 'Pilih Role') }}
                                        {!! Form::select('role', $role, $users->role, ['class'=>'form-control select2']) !!}
                                    </div>
                                </div>
                            </div>

                            <hr>



                            <div class="row mt-3">
                                <div class="col-xl-6">
                                    <a href="{{ url()->previous() }}" class="btn btn-warning btn-sm">Kembali</a>
                                </div>
                                <div class="col-xl-6 text-end">
                                    <button class="btn btn-primary btn-sm" type="submit">Simpan</button>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>


            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
    <!-- Container-fluid Ends-->
</div>

@endsection
@section('page-js')

<script>
    $(document).ready(function() {
        $('.select2').select2();
    });
</script>

@endsection
