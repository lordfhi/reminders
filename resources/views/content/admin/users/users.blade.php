@extends('layouts.master')
@section('judul', 'Users')
@section('page-css')

@endsection
@section('content')

<div class="page-body">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-sm-6 mt-5">
                    <h3>{{ $judul }}</h3>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                        <li class="breadcrumb-item active">Users</li>
                    </ol>
                </div>
                <div class="col-lg-6 text-end">
                    <a href="{{ route('users_tambah') }}" class="btn btn-primary btn-sm">Buat Users</a>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table">
                            <table class="display" id="basic-1">
                                <thead>
                                    <tr>
                                        <th>Nama Pegawai</th>
                                        <th>Email</th>
                                        <th width="300">Role</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($data as $rows)
                                    <tr>
                                        <td>{{$rows->nama}}</td>
                                        <td>{{$rows->email}}</td>
                                        <td>{{$rows->roles->nama_role}}</td>
                                        <td class="text-center">
                                            <a href="{{ route('users_lihat',$rows->id) }}" class="btn btn-secondary btn-xs">Lihat Dosir Digital</a>
                                            <a href="{{ route('users_edit',$rows->id) }}" class="btn btn-warning btn-xs">Ubah</a>
                                            <a href="{{ route('users_delete',$rows->id) }}" class="btn btn-danger btn-xs">Hapus</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
</div>

@endsection
@section('page-js')

@endsection
