@extends('layouts.master')
@section('judul', 'Users')
@section('page-css')

@endsection
@section('content')

<div class="page-body">
    <div class="container-fluid mt-3">
        <div class="page-header">
            <div class="row">
                <div class="col-sm-6">
                    <h3>{{ $judul }}</h3>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                        <li class="breadcrumb-item">Users</li>
                        <li class="breadcrumb-item active">Formulir Users</li>
                    </ol>
                </div>
                <div class="col-lg-6 text-end">
                    {{-- <button type="button" class="btn btn-primary btn-sm">Buat Permohonan</button>  --}}
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">

                        <div class="row mb-3">
                            <div class="col-xl-12">
                                <div class="mb-3">
                                    {{ Form::label('layanan', 'Nama Pegawai') }}
                                    {{ Form::text('nama_pegawai' , $users->nama, ['class'=>'form-control', 'placeholder'=>'Masukan Nama Pegawai','readonly']) }}
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-xl-12">
                                <div class="mb-3">
                                    {{ Form::label('layanan', 'NIP') }}
                                    {{ Form::text('nip' , $users->nip, ['class'=>'form-control', 'placeholder'=>'Masukan NIP Pegawai','readonly']) }}
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-xl-12">
                                <div class="mb-3">
                                    {{ Form::label('layanan', 'Email') }}
                                    {{ Form::text('email' , $users->email, ['class'=>'form-control', 'placeholder'=>'Masukan Email Pegawai','readonly']) }}
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-xl-12">
                                <div class="mb-3">
                                    {{ Form::label('layanan', ' Role') }}
                                    {!! Form::text('role', $users->roles->nama_role, ['class'=>'form-control','disabled']) !!}
                                </div>
                            </div>
                        </div>

                        <hr>
                        <div class="row">
                            <div class="col-md-6">
                               <div class="card">
                                <div class="card-header">
                                    <h5>SK PNS</h5>
                                </div>
                                    <embed src="{{Storage::url($dosir->sk_pns)}}" width="100%" height="500px;" />
                               </div>
                                </div>
                                <div class="col-md-6">
                                 <div class="card">
                                    <div class="card-header">
                                        <h5>SK CPNS</h5>
                                     </div>
                                        <embed src="{{Storage::url($dosir->sk_cpns)}}" width="100%" height="500px;" />
                                 </div>
                                </div>
                                <div class="col-md-6">
                                   <div class="card">
                                    <div class="card-header">
                                        <h5>ANGKA KREDIT</h5>
                                    </div>
                                    <embed src="{{Storage::url($dosir->ak)}}" width="100%" height="500px;" />
                                   </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="card">
                                            <div class="card-header">
                                                <h5>TASPEN</h5>
                                            </div>
                                            <embed src="{{Storage::url($dosir->taspen)}}" width="100%" height="500px;" />
                                        </div>
                                        </div>
                                        <div class="col-md-6">
                                           <div class="card">
                                            <div class="card-header">
                                                <h5>KARTU PEGAWAI</h5>
                                            </div>
                                            <embed src="{{Storage::url($dosir->kartu_pegawai)}}" width="100%" height="500px;" />
                                           </div>
                                            </div>
                                        </div>
                                        <div class="row mt-3">
                                            <div class="col-xl-6">
                                                <a href="{{ url()->previous() }}" class="btn btn-warning btn-sm">Kembali</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                        <!-- Container-fluid Ends-->
                        <!-- Container-fluid Ends-->
                    </div>

                    @endsection
                    @section('page-js')

                    <script>
                        $(document).ready(function() {
                            $('.select2').select2();
                        });
                    </script>

                    @endsection
