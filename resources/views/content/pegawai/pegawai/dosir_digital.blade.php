@extends('layouts.master')
@section('judul', 'SAELAKONA - DOSIR DIGITAL')
@section('page-css')

@endsection
@section('content')

<div class="page-body">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-sm-6 mt-5">
                    <h3>{{ $judul }}</h3>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                        <li class="breadcrumb-item active">DOSIR DIGITAL</li>
                    </ol>
                </div>
                <div class="col-lg-6 text-end">
                    <a href="{{ route('dosirdigital.add') }}" class="btn btn-primary btn-sm">Buat Dosir Digital</a>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        @if(is_array($reminder))
						<div class="alert alert-{{ $reminder['type'] }}"> &nbsp; Reminder : {{ $reminder['msg'] }}</div> 
                        @endif
                        <div class="table">
                            <table class="display" id="basic-1">
                                <thead>
                                    <tr>
                                        <th>NIP</th>
                                        <th>Waktu</th>
                                        <th width="300">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                   @foreach ( $data as $row)
                                   <tr>
                                    <td>{{$row->user->nip}}</td>
                                    <td>{{$row->created_at}}</td>
                                    <td class="text-center">
                                        <a href="{{ route('dosirdigital.view',$row->id) }}" class="btn btn-secondary btn-xs">Lihat</a>
                                        <a href="{{ route('dosirdigital.view',$row->id) }}" class="btn btn-warning btn-xs">Ubah</a>
                                        <a href="{{route('dosirdigital.delete',$row->id)}}" class="btn btn-danger btn-xs">Hapus</a>
                                    </td>
                                </tr>
                                   @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
</div>

@endsection
@section('page-js')

@endsection
