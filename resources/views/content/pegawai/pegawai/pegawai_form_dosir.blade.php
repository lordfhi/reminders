@extends('layouts.master') @section('judul', 'SAELAKONA - Dosir Digital') @section('page-css') @endsection @section('content')
<div class="page-body">
	<div class="container-fluid">
		<div class="page-header">
			<div class="row">
				<div class="col-sm-6 mt-5">
					<h3>{{ $judul }}</h3>
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
						<li class="breadcrumb-item">Dosir Digital</li>
						<li class="breadcrumb-item active">Formulir Dosir Digital</li>
					</ol>
				</div>
				<div class="col-lg-6 text-end"> {{--
					<button type="button" class="btn btn-primary btn-sm">Buat Permohonan</button> --}} </div>
			</div>
		</div>
	</div>
	<!-- Container-fluid starts-->
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-12">
				<div class="card">
					<div class="card-body"> {!! Form::open(['route' => ['dosirdigital.save'],'files'=>true , 'class'=>'theme-form']) !!} @csrf
						<input type="hidden" name="id" value="{{$data->id}}"> @if($errors->any())
						<div class="alert alert-danger"> &nbsp; Validation Failed :
							<br/>
							<ul class="p-l-20"> @foreach ($errors->all() as $error)
								<li>{{ ucfirst($error) }}</li> @endforeach </ul>
						</div> @endif
						<div class="row mb-3">
							<div class="col-xl-6">
								<div class="mb-3"> {{ Form::label('layanan', 'NAMA') }} {{ Form::text('nama',strtoupper(Auth()->user()->nama), ['class'=>'form-control','readonly']) }}</div>
							</div>
							<div class="col-xl-6">
								<div class="mb-3"> {{ Form::label('layanan', 'NIP') }} {{ Form::text('nip',Auth()->user()->nip, ['class'=>'form-control','readonly']) }}</div>
							</div>
						</div>

						<div class="row mb-3">
							<div class="col-xl-6">
								<div class="mb-3"> {{ Form::label('layanan', 'Tanggal SK PNS') }} {{ Form::date('tgl_sk_pns',$data->tgl_sk_pns, ['class'=>'form-control']) }} </div>
							</div>
							<div class="col-xl-6">
								<div class="mb-3"> {{ Form::label('layanan', 'Angka Kredit (AK)') }} {{ Form::text('ak',$data->ak, ['class'=>'form-control']) }} </div>
							</div>
						</div>
                        <div class="row mb-3">
							<div class="col-xl-6">
								<div class="mb-3"> {{ Form::label('layanan', 'Pangkat') }}
                                    {{ Form::select('pangkat',$pangkat, 'null',['class'=>'form-control','readonly']) }}
								</div>
							</div>
							<div class="col-xl-6">
								<div class="mb-3"> {{ Form::label('layanan', 'Golongan') }}
									{{ Form::select('golongan',$golongan, $data->golongan ,['class'=>'form-control']) }}
								</div>
							</div>
						</div>
						<div class="row mb-3">
							<div class="col-xl-6">
								<div class="mb-3"> {{ Form::label('layanan', 'SK_CPNS') }} {{ Form::file('sk_cpns', ['class'=>'form-control']) }}
									<br> @if (empty($data->sk_cpns)) @else <a href="{{Storage::url($data->sk_cpns)}}" target="_blank" class="btn btn-sm btn-primary"><i data-feather="download"></i> &nbsp; Download</a> @endif </div>
							</div>
							<div class="col-xl-6">
								<div class="mb-3"> {{ Form::label('layanan', 'SK_PNS') }} {{ Form::file('sk_pns', ['class'=>'form-control']) }}
									<br> @if (empty($data->sk_pns)) @else
									<input type="hidden" name="id" value="{{$data->id}}"> <a href="{{Storage::url($data->sk_pns)}}" target="_blank" class="btn btn-sm btn-primary"><i data-feather="download"></i> &nbsp; Download</a> @endif </div>
							</div>
						</div>
						<div class="row mb-3">
							<div class="col-xl-6">
								<div class="mb-3"> {{ Form::label('layanan', 'SK Kenaikan Pangkat Terakhir') }} {{ Form::file('sk_kenaikan_pangkat_terakhir', ['class'=>'form-control']) }}
									<br> @if (empty($data->sk_kenaikan_pangkat_terakhir)) @else <a href="{{Storage::url($data->sk_kenaikan_pangkat_terakhir)}}" target="_blank" class="btn btn-sm btn-primary"><i data-feather="download"></i> &nbsp; Download</a> @endif </div>
							</div>
							<div class="col-xl-6">
								<div class="mb-3"> {{ Form::label('layanan', 'SK Kenaikan Pengangkatan Pertama Kali dalam JF (Bagi Fungsional)') }} {{ Form::file('sk_kenaikan_pangkat_terakhir_jf', ['class'=>'form-control']) }}
									<br> @if (empty($data->sk_kenaikan_pangkat_terakhir_jf)) @else <a href="{{Storage::url($data->sk_kenaikan_pangkat_terakhir_jf)}}" target="_blank" class="btn btn-sm btn-primary"><i data-feather="download"></i> &nbsp; Download</a> @endif </div>
							</div>
						</div>
						<div class="row mb-3">
							<div class="col-xl-6">
								<div class="mb-3"> {{ Form::label('layanan', 'Surat Pernyataan Melaksanakan Tugas (SPMT)') }} {{ Form::file('spmt', ['class'=>'form-control']) }}
									<br> @if (empty($data->spmt)) @else <a href="{{Storage::url($data->spmt)}}" target="_blank" class="btn btn-sm btn-primary"><i data-feather="download"></i> &nbsp; Download</a> @endif </div>
							</div>
							<div class="col-xl-6">
								<div class="mb-3"> {{ Form::label('layanan', 'Surat Pernyataan Pelantikan (Bagi Fungsional)') }} {{ Form::file('spp', ['class'=>'form-control']) }}
									<br> @if (empty($data->spp)) @else <a href="{{Storage::url($data->spp)}}" target="_blank" class="btn btn-sm btn-primary"><i data-feather="download"></i> &nbsp; Download</a> @endif </div>
							</div>
						</div>
						<div class="row mb-3">
							<div class="col-xl-6">
								<div class="mb-3"> {{ Form::label('layanan', 'Surat Pernyataan Menduduki Jabatan (Bagi Struktural)') }} {{ Form::file('spmj', ['class'=>'form-control']) }}
									<br> @if (empty($data->spmj)) @else <a href="{{Storage::url($data->spmj)}}" target="_blank" class="btn btn-sm btn-primary"><i data-feather="download"></i> &nbsp; Download</a> @endif </div>
							</div>
							<div class="col-xl-6">
								<div class="mb-3"> {{ Form::label('layanan', 'Riwayat Penetapan Angka Kredit') }} {{ Form::file('rpak', ['class'=>'form-control']) }}
									<br> @if (empty($data->rpak)) @else <a href="{{Storage::url($data->rpak)}}" target="_blank" class="btn btn-sm btn-primary"><i data-feather="download"></i> &nbsp; Download</a> @endif </div>
							</div>
						</div>

						<div class="row mb-3">
							<div class="col-xl-6">
								<div class="mb-3"> {{ Form::label('layanan', 'Kartu Pegawai') }} {{ Form::file('kartu_pegawai', ['class'=>'form-control']) }}
									<br> @if (empty($data->kartu_pegawai)) @else <a href="{{Storage::url($data->kartu_pegawai)}}" target="_blank" class="btn btn-sm btn-primary"><i data-feather="download"></i> &nbsp; Download</a> @endif </div>
							</div>
							<div class="col-xl-6">
								<div class="mb-3"> {{ Form::label('layanan', 'TASPEN') }} {{ Form::file('taspen', ['class'=>'form-control']) }}
									<br> @if (empty($data->taspen)) @else <a href="{{Storage::url($data->taspen)}}" target="_blank" class="btn btn-sm btn-primary"><i data-feather="download"></i> &nbsp; Download</a> @endif </div>
							</div>
						</div>
						<div class="row mb-3">
							<div class="col-xl-6">
								<div class="mb-3"> {{ Form::label('layanan', 'Ijazah Terakhir') }} {{ Form::file('ijazah_terakhir', ['class'=>'form-control']) }}
									<br> @if (empty($data->ijazah_terakhir)) @else <a href="{{Storage::url($data->ijazah_terakhir)}}" target="_blank" class="btn btn-sm btn-primary"><i data-feather="download"></i> &nbsp; Download</a> @endif </div>
							</div>
							<div class="col-xl-6">
								<div class="mb-3"> {{ Form::label('layanan', 'SKP 2021') }} {{ Form::file('skp_2021', ['class'=>'form-control']) }}
									<br> @if (empty($data->skp_2021)) @else <a href="{{Storage::url($data->skp_2021)}}" target="_blank" class="btn btn-sm btn-primary"><i data-feather="download"></i> &nbsp; Download</a> @endif </div>
							</div>
						</div>
						<div class="row mb-3">
							<div class="col-xl-6">
								<div class="mb-3"> {{ Form::label('layanan', 'KTP (Kartu Tanda Penduduk)') }} {{ Form::file('ktp', ['class'=>'form-control']) }}
									<br> @if (empty($data->ktp)) @else <a href="{{Storage::url($data->ktp)}}" target="_blank" class="btn btn-sm btn-primary"><i data-feather="download"></i> &nbsp; Download</a> @endif </div>
							</div>
							<div class="col-xl-6">
								<div class="mb-3"> {{ Form::label('layanan', 'KK (Kartu Keluarga)') }} {{ Form::file('kk', ['class'=>'form-control']) }}
									<br> @if (empty($data->kk)) @else <a href="{{Storage::url($data->kk)}}" target="_blank" class="btn btn-sm btn-primary"><i data-feather="download"></i> &nbsp; Download</a> @endif </div>
							</div>
						</div>
                        <div class="row mb-3">
                            <div class="col-xl-12">
                                <div class="card ribbon-wrapper">
                                    <div class="card-body" id="area_file_pendukung">
                                        <div class="ribbon ribbon-primary ribbon-space-bottom">Data Pendukung</div>
										<input type="hidden" name="deleted_file_pendukung_id" id="deleted_file_pendukung_id">
										@if($file_tambahan != null)
											@foreach($file_tambahan as $i => $file)
											<div class="row mb-3 {{ $i == 0 ? '' : 'additional-field' }}">
												<div class="col-xl-6">
													<div class="mb-3"> {{ Form::label('layanan', 'Nama File') }} {{ Form::text('nama_file[]',$file->nama_file, ['class'=>'form-control']) }} </div>
												</div>
												<div class="col-xl-5">
													<div class="mb-3"> 
														{{ Form::label('layanan', 'File') }} {{ Form::file('file_tambahan[]', ['class'=>'form-control']) }}
														<br> @if (empty($file->file_tambahan)) @else <a href="{{Storage::url($file->file_tambahan)}}" target="_blank" class="btn btn-sm btn-primary"><i data-feather="download"></i> &nbsp; Download</a> @endif 
														<input type="hidden" name="file_tambahan_id[]" value="{{ $file->id }}" class="file-tambahan-id">
													</div>
												</div>
												<div class="col-xl-1">
													<label for="">&nbsp;</label>
													<button type="button" class="btn {{ $i == 0 ? 'btn-success btn-add' : 'btn-danger btn-delete'}}"><i class="fa fa-{{ $i == 0 ? 'plus' : 'trash'}}"></i></button>
												</div>
											</div>
											@endforeach
										@else
										<div class="row mb-3">
                                            <div class="col-xl-6">
                                                <div class="mb-3"> {{ Form::label('layanan', 'Nama File') }} {{ Form::text('nama_file[]',null, ['class'=>'form-control']) }} </div>
                                            </div>
                                            <div class="col-xl-5">
                                                <div class="mb-3"> {{ Form::label('layanan', 'File') }} {{ Form::file('file_tambahan[]', ['class'=>'form-control']) }} </div>
                                            </div>
                                            <div class="col-xl-1">
                                                <label for="">&nbsp;</label>
                                                <button type="button" class="btn btn-success btn-add"><i class="fa fa-plus"></i></button>
                                            </div>
                                        </div>
										@endif
                                    </div>
                                </div>
                            </div>
                        </div>
						<hr>
						<div class="row mt-3">
							<div class="col-xl-6"> <a href="{{ url()->previous() }}" class="btn btn-warning btn-sm">Kembali</a> </div>
							<div class="col-xl-6 text-end">
								<button type="submit" class="btn btn-primary btn-sm">Simpan</button> {{-- <a href="{{ route('layanan') }}" class="btn btn-primary btn-sm">Simpan</a> --}} </div>
						</div> {!! Form::close() !!} </div>
				</div>
			</div>
		</div>
	</div>
	<!-- Container-fluid Ends-->
	<!-- Container-fluid Ends-->
</div> @endsection @section('page-js')
<script>
$(document).ready(function() {
	$('.select2').select2();

    let field_file_pendukung = '<div class="row mb-3 additional-field">\n' +
        '                                            <div class="col-xl-6">\n' +
        '                                                <div class="mb-3"> {{ Form::label("layanan", "Nama File") }} {{ Form::text("nama_file[]",null, ["class"=>"form-control"]) }} </div>\n' +
        '                                            </div>\n' +
        '                                            <div class="col-xl-5">\n' +
        '                                                <div class="mb-3"> {{ Form::label("layanan", "File") }} {{ Form::file("file_tambahan[]", ["class"=>"form-control"]) }} </div>\n' +
        '                                            </div>\n' +
        '                                            <div class="col-xl-1">\n' +
        '                                                <label for="">&nbsp;</label>\n' +
        '                                                <button type="button" class="btn btn-danger btn-delete"><i class="fa fa-trash"></i></button>\n' +
        '                                            </div>\n' +
        '                                        </div>'
    let test = "ha";

    $('.btn-add').click(function() {
        $('#area_file_pendukung').append(field_file_pendukung);
    })

    $('body').on('click', '.btn-delete', function() {
		let parent = $(this).parents('.additional-field'),
			file_tambahan_id = parent.find('.file-tambahan-id').val();

		if (file_tambahan_id !== undefined) {
			let deleted_id = $('#deleted_file_pendukung_id').val();
			$('#deleted_file_pendukung_id').val(deleted_id + "," + file_tambahan_id);
		}
		
        parent.remove();
    })
});

</script>
<script src="{{asset('')}}assets/js/editor/ckeditor/ckeditor.js"></script>
<script src="{{asset('')}}assets/js/editor/ckeditor/adapters/jquery.js"></script>
<script src="{{asset('')}}assets/js/editor/ckeditor/styles.js"></script>
<script src="{{asset('')}}assets/js/editor/ckeditor/ckeditor.custom.js"></script> @endsection
