@extends('layouts.master')
@section('judul', 'SAELAKONA - Laporan Pekerjaan')
@section('page-css')

@endsection
@section('content')

<div class="page-body">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-sm-6 mt-5">
                    <h3>{{ $judul }}</h3>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                        <li class="breadcrumb-item active">Laporan Pekerjaan</li>
                    </ol>
                </div>
                <div class="col-lg-6 text-end">
                    {{-- <a href="{{ route('layanan_tambah') }}" class="btn btn-primary btn-sm">Buat Laporan Pekerjaan</a> --}}
                </div>
            </div>
        </div>
    </div>
 @if (empty($emonev) )
 <div class="container-fluid">
    <div class="alert alert-danger dark alert-dismissible fade show" role="alert"><strong>Reminder !</strong> Silahkan Mengisi Aplikasi E-MONEV.
        <button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
      </div>
  </div>
  @else
    @if ($emonev->count() < 2)
    <div class="container-fluid">
        <div class="alert alert-danger dark alert-dismissible fade show" role="alert"><strong>Reminder !</strong> Silahkan Mengisi Aplikasi E-MONEV.
            <button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
          </div>
      </div>
    @endif
  @endif
  @if (empty($ekilap))
  <div class="container-fluid">
    <div class="alert alert-danger dark alert-dismissible fade show" role="alert"><strong>Reminder !</strong> Silahkan Mengisi Aplikasi E-KILAP.
        <button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
      </div>
  </div>
  @endif
  @if (empty($surat))
  <div class="container-fluid">
    <div class="alert alert-danger dark alert-dismissible fade show" role="alert"><strong>Reminder !</strong> Silahkan Mengisi Aplikasi Persuratan.
        <button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
      </div>
  </div>
 @endif
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            @foreach ($layanan as $row)
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-body">
                            <img src="{{asset('assets/images/favicons.png')}}" alt="" style="float: left"> &nbsp;&nbsp;&nbsp;&nbsp; <h5 style="float: left;margin-left:3%">{{$row->nama_layanan}}</h5>
                            <br>
                            <hr>
                            Untuk membuat Aktifitas pekerjaan silahkan klik <a href="{{route('reminder_aplikasi',$row->id)}}" class="btn btn-sm btn-primary">Disini</a>
                        </div>
                    </div>
                </div>
            @endforeach
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table">
                            <table class="display" id="basic-1">
                                <thead>
                                    <tr>
                                        <th>Aplikasi</th>
                                        <th>Waktu Upload</th>
                                        <th width="300">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                   @foreach ( $reminder as $row)
                                   <tr>
                                    <td>{{$row->kategori->nama_aplikasi}}</td>
                                    <td>{{$row->waktu_pengisian}}</td>
                                    <td class="text-center">
                                        <a href="{{ route('reminder_lihat',$row->id) }}" class="btn btn-secondary btn-xs">Lihat</a>
                                        <a href="{{ route('reminder_ubah',$row->id) }}" class="btn btn-warning btn-xs">Ubah</a>
                                        <a href="{{ route('reminder_hapus',$row->id) }}" class="btn btn-danger btn-xs">Hapus</a>
                                    </td>
                                </tr>
                                   @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
</div>

@endsection
@section('page-js')

@endsection
