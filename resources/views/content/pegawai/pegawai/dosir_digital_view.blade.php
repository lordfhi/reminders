@extends('layouts.master')
@section('judul', 'SAELAKONA - Dosir Digital')
@section('page-css')

@endsection
@section('content')

<div class="page-body">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-sm-6 mt-5">
                    <h3>{{ $judul }}</h3>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                        <li class="breadcrumb-item">Dosir Digital</li>
                        <li class="breadcrumb-item active">Formulir Dosir Digital</li>
                    </ol>
                </div>
                <div class="col-lg-6 text-end">
                    {{-- <button type="button" class="btn btn-primary btn-sm">Buat Permohonan</button>  --}}
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        {!! Form::open(['route' => ['dosirdigital.save'],'files'=>true , 'class'=>'theme-form']) !!}
                            @csrf

                            @if($errors->any())
                            <div class="alert alert-danger">
                                &nbsp; Validation Failed : <br/>
                                    <ul class="p-l-20">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ ucfirst($error) }}</li>
                                    @endforeach
                                    </ul>
                            </div>
                            @endif
                            <div class="row mb-3">
                                <div class="col-xl-12">
                                    <div class="mb-3">
                                        {{ Form::label('layanan', 'SK_CPNS') }}
                                        {{ Form::file('sk_cpns', ['class'=>'form-control']) }}
                                        <br>
                                        @if (empty($data->sk_cpns))
                                        @else
                                        <a href="{{Storage::url($data->sk_cpns)}}" target="_blank" class="btn btn-sm btn-primary"><i data-feather="download"></i> &nbsp; Download</a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-xl-12">
                                    <div class="mb-3">
                                        {{ Form::label('layanan', 'SK_PNS') }}
                                        {{ Form::file('sk_pns', ['class'=>'form-control']) }}
                                        <br>
                                        @if (empty($data->sk_cpns))
                                        @else
                                        <a href="{{Storage::url($data->sk_cpns)}}" target="_blank" class="btn btn-sm btn-primary"><i data-feather="download"></i> &nbsp; Download</a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-xl-12">
                                    <div class="mb-3">
                                        {{ Form::label('layanan', 'AK') }}
                                        {{ Form::file('ak', ['class'=>'form-control']) }}
                                        <br>
                                        @if (empty($data->ak))
                                        @else
                                        <a href="{{Storage::url($data->ak)}}" target="_blank" class="btn btn-sm btn-primary"><i data-feather="download"></i> &nbsp; Download</a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-xl-12">
                                    <div class="mb-3">
                                        {{ Form::label('layanan', 'TASPEN') }}
                                        {{ Form::file('taspen', ['class'=>'form-control']) }}
                                        <br>
                                        @if (empty($data->taspen))
                                        @else
                                        <a href="{{Storage::url($data->taspen)}}" target="_blank" class="btn btn-sm btn-primary"><i data-feather="download"></i> &nbsp; Download</a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-xl-12">
                                    <div class="mb-3">
                                        {{ Form::label('layanan', 'KARTU PEGAWAI') }}
                                        {{ Form::file('kartu_pegawai', ['class'=>'form-control']) }}
                                        <br>
                                        @if (empty($data->kartu_pegawai))
                                        @else
                                        <a href="{{Storage::url($data->kartu_pegawai)}}" target="_blank" class="btn btn-sm btn-primary"><i data-feather="download"></i> &nbsp; Download</a>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <hr>

                            <div class="row mt-3">
                                <div class="col-xl-6">
                                    <a href="{{ url()->previous() }}" class="btn btn-warning btn-sm">Kembali</a>
                                </div>
                                <div class="col-xl-6 text-end">
                                    <button type="submit" class="btn btn-primary btn-sm">Simpan</button>
                                    {{-- <a href="{{ route('layanan') }}" class="btn btn-primary btn-sm">Simpan</a> --}}
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
    <!-- Container-fluid Ends-->
</div>

@endsection
@section('page-js')

<script>
    $(document).ready(function() {
        $('.select2').select2();
    });
</script>
<script src="{{asset('')}}assets/js/editor/ckeditor/ckeditor.js"></script>
<script src="{{asset('')}}assets/js/editor/ckeditor/adapters/jquery.js"></script>
<script src="{{asset('')}}assets/js/editor/ckeditor/styles.js"></script>
<script src="{{asset('')}}assets/js/editor/ckeditor/ckeditor.custom.js"></script>
@endsection
