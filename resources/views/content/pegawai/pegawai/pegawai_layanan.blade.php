@extends('layouts.master')
@section('judul', 'SAELAKONA - Laporan Pekerjaan')
@section('page-css')

@endsection
@section('content')

<div class="page-body">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-sm-6 mt-5">
                    <h3>{{ $judul }}</h3>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                        <li class="breadcrumb-item active">Laporan Pekerjaan</li>
                    </ol>
                </div>
                <div class="col-lg-6 text-end">
                    {{-- <a href="{{ route('layanan_tambah') }}" class="btn btn-primary btn-sm">Buat Laporan Pekerjaan</a> --}}
                </div>
            </div>
        </div>
    </div>
  <div class="container-fluid">
    <div class="alert alert-danger dark alert-dismissible fade show" role="alert"><strong>Reminder !</strong> Silahkan Mengisi Aplikasi E-monev.
        <button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
      </div>
  </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            @foreach ($layanan as $row)
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-body">
                            <img src="{{asset('assets/images/favicons.png')}}" alt="" style="float: left"> &nbsp;&nbsp;&nbsp;&nbsp; <h5 style="float: left;margin-left:3%">{{$row->nama_aplikasi}}</h5>
                            <br>
                            <hr>
                            {{-- {!! Str::limit($row->keterangan,50) !!} --}}
                            {{-- {!! $row->keterangan !!} --}}

                            <br>
                            Untuk membuat Aktifitas pekerjaan silahkan klik <a href="{{route('reminder_tambah',$row->id)}}" class="btn btn-sm btn-primary">Disini</a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    <!-- Container-fluid Ends-->
</div>

@endsection
@section('page-js')

@endsection
