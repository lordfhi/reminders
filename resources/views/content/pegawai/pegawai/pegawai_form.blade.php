@extends('layouts.master')
@section('judul', 'SAELAKONA - Laporan Pekerjaan')
@section('page-css')

@endsection
@section('content')

<div class="page-body">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-sm-6 mt-5">
                    <h3>{{ $judul }}</h3>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                        <li class="breadcrumb-item">Laporan Pekerjaan</li>
                        <li class="breadcrumb-item active">Formulir Laporan Pekerjaan</li>
                    </ol>
                </div>
                <div class="col-lg-6 text-end">
                    {{-- <button type="button" class="btn btn-primary btn-sm">Buat Permohonan</button>  --}}
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <div class="container-fluid">
                <div class="alert alert-danger dark alert-dismissible fade show" role="alert"><strong>Reminder !</strong> Silahkan Mengisi Aplikasi E-monev.
                    <button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>
                  </div>
              </div>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <img src="{{asset('assets/images/favicons.png')}}" alt="">&nbsp; {{$tutorial->kategori->nama_layanan}}
                        <hr>
                        <h5>Tutorial : </h5>
                        {!! $tutorial->keterangan !!}

                       @if (empty($tutorial->file))

                       @else
                       <video width="100%" height="500" controls>
                        <source src="movie.mp4" type="video/mp4">
                        <source src="movie.ogg" type="video/ogg">
                      Your browser does not support the video tag.
                      </video>
                       @endif
                    </div>
                </div>
            </div>

            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        {!! Form::open(['route' => ['reminder_simpan'],'files'=>true , 'class'=>'theme-form']) !!}
                            @csrf

                            @if($errors->any())
                            <div class="alert alert-danger">
                                &nbsp; Validation Failed : <br/>
                                    <ul class="p-l-20">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ ucfirst($error) }}</li>
                                    @endforeach
                                    </ul>
                            </div>
                            @endif
                            {{ Form::hidden('id_layanan', $tutorial->id_layanan) }}
                            {{ Form::hidden('aplikasi', $tutorial->kategori->nama_aplikasi) }}
                            <div class="row mb-3">
                                <div class="col-xl-12">
                                    <div class="mb-3">
                                        {{ Form::label('layanan', 'Bukti Upload') }}
                                        {{ Form::file('file_laporan', ['class'=>'form-control']) }}
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-xl-12">
                                    <div class="mb-3">
                                        {{ Form::label('keterangan', 'Keterangan') }}
                                        <textarea id="editor1" name="keterangan" cols="30" rows="10"></textarea>
                                    </div>
                                </div>
                            </div>

                            <hr>

                            <div class="row mt-3">
                                <div class="col-xl-6">
                                    <a href="{{ url()->previous() }}" class="btn btn-warning btn-sm">Kembali</a>
                                </div>
                                <div class="col-xl-6 text-end">
                                    <button type="submit" class="btn btn-primary btn-sm">Simpan</button>
                                    {{-- <a href="{{ route('layanan') }}" class="btn btn-primary btn-sm">Simpan</a> --}}
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
    <!-- Container-fluid Ends-->
</div>

@endsection
@section('page-js')

<script>
    $(document).ready(function() {
        $('.select2').select2();
    });
</script>
<script src="{{asset('')}}assets/js/editor/ckeditor/ckeditor.js"></script>
<script src="{{asset('')}}assets/js/editor/ckeditor/adapters/jquery.js"></script>
<script src="{{asset('')}}assets/js/editor/ckeditor/styles.js"></script>
<script src="{{asset('')}}assets/js/editor/ckeditor/ckeditor.custom.js"></script>
@endsection
