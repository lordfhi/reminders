<?php

use Illuminate\Http\Request;
use App\Models\AppSerialMaster;
use App\Models\AppSerial;

if (! function_exists('nomorPRSKNT')) {
    function nomorPRSKNT($nama_tabel = null, $prefix = null)
    {
        $bulan = date('m');
        $tahun = date('y');
        $serial = AppSerialMaster::where([
            'tabel' => $nama_tabel,
            'tahun' => $tahun,
            'bulan' => $bulan,
            'prefix' => $prefix
        ])->orderBy('nilai', 'desc')->first();

        if($serial){
            $nilai = $serial->nilai;
            $serial->nilai = $nilai+1;
            $serial->save();
            $nomor_baru = $serial->nilai;
        }
        else{
            $nomor_baru = 1;
            $serial = new AppSerialMaster();
            $serial->tabel = $nama_tabel;
            $serial->tahun = $tahun;
            $serial->bulan = $bulan;
            $serial->prefix = $prefix;
            $serial->nilai = $nomor_baru;
            $serial->save();
        }

        $nomor = $prefix.'.'.$tahun.$bulan.'.'.str_repeat('0',(6 - strlen($nomor_baru))).$nomor_baru;
        return strtoupper($nomor);
    }
}

if (! function_exists('nomorByPort')) {
    function nomorByPort($jns_dokumen = null, $pelabuhan_kode = null)
    {
        $bulan = date('m');
        $tahun = date('y');
        $serial = AppSerial::where([
            'jns_dokumen' => $jns_dokumen,
            'tahun' => $tahun,
            'bulan' => $bulan,
            'pelabuhan_kode' => $pelabuhan_kode
        ])->orderBy('nilai', 'desc')->first();

        if($serial){
            $nilai = $serial->nilai;
            $serial->nilai = $nilai+1;
            $serial->save();
            $nomor_baru = $serial->nilai;
        }
        else{
            $nomor_baru = 1;
            $serial = new AppSerial();
            $serial->jns_dokumen = $jns_dokumen;
            $serial->tahun = $tahun;
            $serial->bulan = $bulan;
            $serial->pelabuhan_kode = $pelabuhan_kode;
            $serial->nilai = $nomor_baru;
            $serial->save();
        }

        $nomor = $jns_dokumen.'.'.$pelabuhan_kode.'.'.$tahun.$bulan.'.'.str_repeat('0',(6 - strlen($nomor_baru))).$nomor_baru;
        return strtoupper($nomor);
    }
}

if (! function_exists('nomor')) {
    function nomor($jns_dokumen = null, $flag = null, $pelabuhan_kode = null)
    {
        $bulan = date('m');
        $tahun = date('y');
        $serial = AppSerial::where([
            'jns_dokumen' => $jns_dokumen,
            'tahun' => $tahun,
            'bulan' => $bulan,
            'pelabuhan_kode' => $pelabuhan_kode
        ])->orderBy('nilai', 'desc')->first();

        if($serial){
            $nilai = $serial->nilai;
            $serial->nilai = $nilai+1;
            $serial->save();
            $nomor_baru = $serial->nilai;
        }
        else{
            $nomor_baru = 1;
            $serial = new AppSerial();
            $serial->jns_dokumen = $jns_dokumen;
            $serial->tahun = $tahun;
            $serial->bulan = $bulan;
            $serial->pelabuhan_kode = $pelabuhan_kode;
            $serial->nilai = $nomor_baru;
            $serial->save();
        }

        $nomor = $jns_dokumen.'.'.$flag.'.'.$pelabuhan_kode.'.'.$tahun.$bulan.'.'.str_repeat('0',(6 - strlen($nomor_baru))).$nomor_baru;
        return strtoupper($nomor);
    }
}

if (! function_exists('nomorLayanan')) {
    function nomorLayanan($jns_dokumen = null, $pelabuhan_kode = null)
    {
        $bulan = date('m');
        $tahun = date('y');
        $serial = AppSerial::where([
            'jns_dokumen' => $jns_dokumen,
            'tahun' => $tahun,
            'bulan' => $bulan,
            'pelabuhan_kode' => $pelabuhan_kode
        ])->orderBy('nilai', 'desc')->first();

        if($serial){
            $nilai = $serial->nilai;
            $serial->nilai = $nilai+1;
            $serial->save();
            $nomor_baru = $serial->nilai;
        }
        else{
            $nomor_baru = 1;
            $serial = new AppSerial();
            $serial->jns_dokumen = $jns_dokumen;
            $serial->tahun = $tahun;
            $serial->bulan = $bulan;
            $serial->pelabuhan_kode = $pelabuhan_kode;
            $serial->nilai = $nomor_baru;
            $serial->save();
        }

        $nomor = $jns_dokumen.'.'.$pelabuhan_kode.'.'.$tahun.$bulan.'.'.str_repeat('0',(6 - strlen($nomor_baru))).$nomor_baru;
        return strtoupper($nomor);
    }
}

if (! function_exists('nomorSimpaduKapalPindah')) {
    function nomorSimpaduKapalPindah($jns_dokumen, $flag, $pelabuhan_kode)
    {
        $bulan = date('m');
        $tahun = date('y');
        $serial = AppSerial::where([
            'jns_dokumen' => $jns_dokumen,
            'tahun' => $tahun,
            'bulan' => $bulan,
            'pelabuhan_kode' => $pelabuhan_kode
        ])->orderBy('nilai', 'desc')->first();

        if($serial){
            $nilai = $serial->nilai;
            $serial->nilai = $nilai+1;
            $serial->save();
            $nomor_baru = $serial->nilai;
        }
        else{
            $nomor_baru = 1;
            $serial = new AppSerial();
            $serial->jns_dokumen = $jns_dokumen;
            $serial->tahun = $tahun;
            $serial->bulan = $bulan;
            $serial->pelabuhan_kode = $pelabuhan_kode;
            $serial->nilai = $nomor_baru;
            $serial->save();
        }

        $nomor = $jns_dokumen.'.'.$flag.'.'.$pelabuhan_kode.'.'.$tahun.$bulan.'.'.str_repeat('0',(6 - strlen($nomor_baru))).$nomor_baru;
        return strtoupper($nomor);
    }
}
