<?php

use Illuminate\Http\UploadedFile;

if (!function_exists('__getDataSik')){
    function __getDataSik($function_name, $params){
        $endpoint = "https://sik.dephub.go.id/api/index.php/soap_services/sik_api?wsdl";
        $username = "getdatasik";
        $password = "4dm1n@2021";

        $options = array(
            'login' => $username,
            'password' => $password,
            'trace'=>1
        );
        $client = new \SoapClient($endpoint, $options);
        try {
            $responseData = $client->__soapCall($function_name,$params);
            if($responseData['status']->status == 1){
                $response = [
                    'rc'=>'00',
                    'rcMessage'=>'success',
                    'data'=>$responseData
                ];
            } else {
                $response = [
                    'rc'=>'01',
                    'rcMessage'=>'error',
                    'data'=>$responseData
                ];
            }
        } catch (SoapFault $fault) {
            $response = [
                'rc'=>'99',
                'rcMessage'=>$fault->faultcode."-".$fault->faultstring,
                'data'=>null
            ];
        }
        return $response;
    }
}
