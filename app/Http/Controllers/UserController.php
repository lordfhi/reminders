<?php

namespace App\Http\Controllers;

use App\Models\DosirDigitalModel;
use App\Models\RolesModel;
use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
    public function users ()
    {
        $judul = "Users";
        $data = User::all();
        return view ('content.admin.users.users', compact('judul','data'));
    }

    public function users_tambah ()
    {
        $judul = "Formulir Users";
        $users = new User;
        $role = RolesModel::get()->pluck('nama_role','id');
        return view ('content.admin.users.users_form', compact('judul','users','role'));
    }

    public function users_simpan(Request $request)
    {
        $id = $request->input('id');
        $pw = $request->input('password');

        if(empty($id)){
            $users = new User;
        }else{
            $users = User::find($id);
        }
        if(!empty($pw)){
            $password = bcrypt($pw);
        }else{
            $password = $users->password;
        }

        $users->nama = $request->input('nama_pegawai');
        $users->nip = $request->input('nip');
        $users->email = $request->input('email');
        $users->password = $password;
        $users->role = $request->input('role');
        $users->status = 2;
        $users->save();

        return redirect()->route('users');
    }

    public function users_lihat ($id)
    {
        $judul = "Lihat Users";
        $users = User::find($id);
        $dosir = DosirDigitalModel::where('nip',$users->nip)->first();
        $role = RolesModel::get()->pluck('nama_role','id');
        return view ('content.admin.users.users_lihat', compact('judul','users','role','dosir'));
    }
    public function users_edit ($id)
    {
        $judul = "Lihat Users";
        $users = User::find($id);
        $role = RolesModel::get()->pluck('nama_role','id');
        return view ('content.admin.users.users_form', compact('judul','users','role'));
    }

    public function users_delete($id)
    {
        $users = User::find($id)->delete();
        return redirect()->back();
    }
}
