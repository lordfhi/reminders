<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ReminderModel;
use App\Models\LayananModel;
use App\Models\AplikasiModel;
use App\Models\TutorialModel;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class RemindersController extends Controller
{
    public function reminder ()
    {
        $judul = "Reminder";
        $auth = auth()->user()->id;
        // $waktu = ReminderModel::where('id_users',$auth)->first();
        // $waktu = Carbon::parse($waktu->waktu_pengisian)->format('Y-m-d');
        $reminder = ReminderModel::where('id_users',$auth)->get();
        $ekilap = ReminderModel::where('id_users',$auth)->where('aplikasi','E-KILAP')->whereDate('waktu_pengisian',Carbon::now())->first();
        $emonev = ReminderModel::where('id_users',$auth)->where('aplikasi','E-MONEV')->whereYear('waktu_pengisian',Carbon::now())->get();
        $surat = ReminderModel::where('id_users',$auth)->where('aplikasi','SURAT MASUK PENYIDIKAN')->whereDate('waktu_pengisian',Carbon::now())->first();
        $layanan = LayananModel::all();
        return view ('content.pegawai.pegawai.pegawai', compact('judul','reminder','layanan','ekilap','emonev','surat'));
    }


    public function reminder_layanan($id)
    {
        $judul = "Reminder";
        $layanan = AplikasiModel::where('id_layanan',$id)->get();
        return view ('content.pegawai.pegawai.pegawai_layanan', compact('judul','layanan'));
    }

    public function reminder_tambah ($id)
    {
        $judul = "Formulir Reminder";
        $tutorial = TutorialModel::where('id_layanan',$id)->first();
        return view ('content.pegawai.pegawai.pegawai_form', compact('judul','tutorial'));
    }

    public function reminder_simpan(Request $request)
    {
        // dd($request->all());
        $nama_layanan = $request->input('id_layanan');
        $keterangan = $request->input('keterangan');
        $aplikasi = $request->input('aplikasi');

        $waktu =date('Y-m-d H:i:s');

        $reminder = new ReminderModel();
        $reminder->waktu_pengisian = $waktu;
        $reminder->id_layanan = $nama_layanan;
        $reminder->id_users = Auth()->user()->id;
        $reminder->keterangan = $keterangan;
        $reminder->aplikasi = $aplikasi;
        $reminder->waktu_pengisian = date('Y-m-d H:i;s');
        if($request->hasFile('file_laporan')){
            $path  = $request->file('file_laporan')->storePublicly('public/file_laporan'.date('Y').'/'.date('m').'/'.date("d"));
            $reminder->file = $path;
        }
        $reminder->save();

        return redirect()->route('reminder');
    }

    public function reminder_edit($id)
    {
        $judul = "Edit Reminder";
        $reminder = ReminderModel::find($id);
        return view ('content.pegawai.pegawai.pegawai_edit', compact('judul','reminder'));
    }

    public function reminder_update(Request $request)
    {
        $id = $request->input('id');
        $waktu =date('Y-m-d H:i:s');
        $keterangan = $request->input('keterangan');
        $aplikasi = $request->input('aplikasi');
        $reminder = ReminderModel::find($id);
        $reminder->keterangan = $keterangan;
        $reminder->waktu_pengisian = $waktu;
        $reminder->aplikasi = $aplikasi;
        if($request->hasFile('file_laporan')){
            $path  = $request->file('file_laporan')->storePublicly('public/file_laporan'.date('Y').'/'.date('m').'/'.date("d"));
            $reminder->file = $path;
        }
        $reminder->save();

        return redirect()->route('reminder');
    }

    public function reminder_lihat ($id)
    {
        $judul = "Lihat Laporan Pekerjaan";
        $layanan = ReminderModel::find($id);
        return view ('content.pegawai.pegawai.pegawai_lihat', compact('judul','layanan'));
    }

    public function reminder_hapus($id)
    {
        $hapus = ReminderModel::find($id);
        $hapus->delete();
        return redirect()->route('reminder');
    }

    public function reminder_dosir(Request $request)
    {
        // dd($request->all());
        $id = Auth()->user()->id;
        $user = User::find($id);
        if($request->hasFile('sk_cpns')){
            $path  = $request->file('sk_cpns')->storePublicly('public/sk_cpns'.date('Y').'/'.date('m').'/'.date("d"));
            $user->sk_cpns = $path;
        }
        if($request->hasFile('sk_pns')){
            $path  = $request->file('sk_pns')->storePublicly('public/sk_pns'.date('Y').'/'.date('m').'/'.date("d"));
            $user->sk_pns = $path;
        }
        if($request->hasFile('ak')){
            $path  = $request->file('ak')->storePublicly('public/ak'.date('Y').'/'.date('m').'/'.date("d"));
            $user->ak = $path;
        }
        if($request->hasFile('taspen')){
            $path  = $request->file('taspen')->storePublicly('public/taspen'.date('Y').'/'.date('m').'/'.date("d"));
            $user->taspen = $path;
        }
        if($request->hasFile('kartu_pegawai')){
            $path  = $request->file('kartu_pegawai')->storePublicly('public/kartu_pegawai'.date('Y').'/'.date('m').'/'.date("d"));
            $user->kartu_pegawai = $path;
        }
        $user->save();


        return redirect()->route('reminder');
    }


}
