<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;


class SessionController extends Controller
{


    public function signin ()
    {
        return view('session.signin');
    }

    public function loginProses(Request $request)
    {
        $email = $request->input('email');
        $password = $request->input('password');

        if (Auth::attempt(['email'=>$email,'password'=>$password,'status'=>2])) {
            return redirect()->route('dashboard');
        } else{
            Session::flash("flash_notification", [
                "level" => "danger",
                "message" => "Cek kembali Email dan Password Anda !"
            ]);
            return redirect()->route('login');
        }
    }

    public function logout()
    {
        Session::flush();

        Auth::logout();

        return redirect()->route('login');
    }

    public function signup ()
    {
        return view('session.signup');
    }


}
