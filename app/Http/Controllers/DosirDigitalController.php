<?php

namespace App\Http\Controllers;

use App\Models\FileDosirDigitalModel;
use App\Models\FileTambahanModel;
use Illuminate\Http\Request;
use App\Models\DosirDigitalModel;
use App\Models\ModelPangkat;
use App\Models\ModelGolongan;

class DosirDigitalController extends Controller
{
    public function dosirdigital()
    {
        $user = auth()->user();
        $judul = "Dosir Digital";
        $data = FileDosirDigitalModel::where('created_by', $user->id)->get();
        $last_data = @$data[count($data) - 1];

        if ($last_data != null) {
            $reminder = null;

            if($user->isFungtional()) {
                $years_to_wait = "+3 seconds";
                $next_golongan = ModelGolongan::find($last_data->golongan+1);
                $ak_already_exceed = $next_golongan->ak <= $last_data->ak;
            } else {
                $years_to_wait = "+4 years";
                $ak_already_exceed = true;
            }

            $time_to_promote = strtotime($years_to_wait, strtotime($last_data->tgl_sk_pns ?? 'now')) <= time();

            if ($time_to_promote && $ak_already_exceed) {
                $reminder['msg'] = "Anda berhak mengajukan kenaikan jabatan";
                $reminder['type'] = 'success';
            } else if ($time_to_promote && $ak_already_exceed == false) {
                $reminder['msg'] = "Segera kumpulkan AK";
                $reminder['type'] = 'warning';
            }
        }

        return view('content.pegawai.pegawai.dosir_digital', compact('data', 'judul', 'reminder'));
    }

    public function addDosirDigital()
    {
        $judul = "Dosir Digital";
        $golongan = ModelGolongan::get()->pluck('golongan','id');
        $pangkat = ModelPangkat::get()->pluck('pangkat','id');
        $data = new DosirDigitalModel;
        $file_tambahan = [];
        return view ('content.pegawai.pegawai.pegawai_form_dosir', compact('data','judul','golongan','pangkat', 'file_tambahan'));
    }

    public function reminder_dosir(Request $request)
    {
        $id = $request->input('id');
        $file_attributes = ['sk_cpns', 'sk_pns', 'sk_kenaikan_pangkat_terakhir', 'sk_kenaikan_pangkat_terakhir_jf', 'spmt', 'spp', 'spmj', 'rpak', 'kartu_pegawai', 'taspen', 'ijazah_terakhir', 'skp_2021', 'ktp', 'kk'];

        $validation = [];
        foreach ($file_attributes as $attribute) {
            $validation[$attribute] = 'file|mimes:pdf|max:2000';
        }

        $model = empty($id) ? new FileDosirDigitalModel : FileDosirDigitalModel::query()->find($id);
        $this->validate($request, $validation);

        foreach ($file_attributes as $attribute) {
            if($request->hasFile($attribute)){
                $path  = $request->file($attribute)->storePublicly('public/'.$attribute.date('Y').'/'.date('m').'/'.date("d"));
                $model->$attribute = $path;
            }
        }

        $model->golongan = $request->golongan;
        $model->tgl_sk_pns = $request->tgl_sk_pns;
        $model->ak = $request->ak;
        $model->created_by = auth()->user()->id;
        $model->save();

        $this->saveFileTambahan($model, $request);

        return redirect()->route('dosirdigital');
    }

    private function saveFileTambahan($dosir_digital, $request)
    {
        foreach ($request->nama_file as $i => $value) {
            $file_tambahan_id = @$request->file_tambahan_id[$i];
            $file_tambahan_model = $file_tambahan_id != null ? FileTambahanModel::find($file_tambahan_id) : new FileTambahanModel();
            
            if ($request->hasFile('file_tambahan') && isset($request->file("file_tambahan")[$i])) {
                $path  = $request->file("file_tambahan")[$i]->storePublicly('public/file_tambahan'.date('Y').'/'.date('m').'/'.date("d"));
                $file_tambahan_model->file_tambahan = $path;
            }

            
            $file_tambahan_model->dosir_digital_id = $dosir_digital->id;
            $file_tambahan_model->nama_file = $request->nama_file[$i];
            $file_tambahan_model->created_by = $dosir_digital->created_by;
            $file_tambahan_model->save();
        }

        $deleted_file_tambahan_id = explode(',', $request->deleted_file_pendukung_id);
        foreach ($deleted_file_tambahan_id as $id) {
            if (empty($id)) continue;

            FileTambahanModel::find($id)->delete();                
        }
    }

    public function lihatDosir($id)
    {
        $judul = "Dosir Digital";
        $data = FileDosirDigitalModel::find($id);
        $pangkat = ModelPangkat::get()->pluck('pangkat','id');
        $golongan = ModelGolongan::get()->pluck('golongan','id');
        $file_tambahan = FileTambahanModel::query()->where('dosir_digital_id', $id)->get();

        return view ('content.pegawai.pegawai.pegawai_form_dosir', compact('data','judul','pangkat','golongan', 'file_tambahan'));
    }

    public function hapusDosir($id)
    {
        $data = FileDosirDigitalModel::find($id);
        $data->delete();
        return redirect()->back();
    }

}
