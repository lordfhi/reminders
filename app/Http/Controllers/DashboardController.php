<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\LayananModel;
use App\Models\ReminderModel;
use DB;

class DashboardController extends Controller
{

    public function dashboard ()
    {
        $pegawai = DB::table('users')->count();
        $reminder = DB::table('data_reminders')->count();

        $reminder_all = ReminderModel::all();

        return view('content.dashboard.dashboard',compact('pegawai','reminder','reminder_all'));
    }


}
