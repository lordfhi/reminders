<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\LayananModel;

use App\Models\AplikasiModel;
class DataMasterLayanan extends Controller
{
    public function layanan ()
    {
        $judul = "Kategori";
        $layanan = LayananModel::all();
        return view ('content.admin.layanan.layanan', compact('judul','layanan'));
    }

    public function layanan_tambah ()
    {
        $judul = "Formulir Kategori";
        return view ('content.admin.layanan.layanan_form', compact('judul'));
    }

    public function layanan_simpan(Request $request)
    {
        // dd($request->all());
        $nama_layanan = $request->input('layanan');
        $keterangan = $request->input('keterangan');

        $layanan = new LayananModel();
        $layanan->nama_layanan = $nama_layanan;
        $layanan->keterangan = $keterangan;
        $layanan->save();

        return redirect()->route('layanan');
    }

    public function layanan_edit($id)
    {
        $judul = "Edit Kategori";
        $layanan = LayananModel::find($id);
        return view ('content.admin.layanan.layanan_edit', compact('judul','layanan'));
    }

    public function layanan_update(Request $request)
    {
        $id = $request->input('id');
        $nama_layanan = $request->input('layanan');
        $keterangan = $request->input('keterangan');
        $layanan = LayananModel::find($id);
        $layanan->nama_layanan = $nama_layanan;
        $layanan->keterangan = $keterangan;
        $layanan->save();

        return redirect()->route('layanan');
    }

    public function layanan_lihat ($id)
    {
        $judul = "Lihat Kategori";
        $layanan = LayananModel::find($id);
        return view ('content.admin.layanan.layanan_lihat', compact('judul','layanan'));
    }

    public function layanan_hapus($id)
    {
        $hapus = LayananModel::find($id);
        $hapus->delete();
        return redirect()->route('layanan');
    }

    public function aplikasi($id)
    {
        $judul = "List Aplikasi";
        $aplikasi = AplikasiModel::where('id_layanan',$id)->get();
        return view('content.admin.aplikasi.aplikasi',compact('judul','aplikasi','id'));
    }

    public function aplikasi_tambah($id)
    {
        $judul = "Tambah Aplikasi";
        return view('content.admin.aplikasi.aplikasi_form',compact('judul','id'));
    }


    public function aplikasi_simpan(Request $request)
    {
        $aplikasi = $request->input('aplikasi');
        $id_layanan = $request->input('id');
        // dd($request->all());
        $aplikasi_new = new AplikasiModel();
        $aplikasi_new->nama_aplikasi = $aplikasi;
        $aplikasi_new->id_layanan = $id_layanan;
        $aplikasi_new->save();

        return redirect()->route('aplikasi',$id_layanan);
    }

    public function aplikasi_lihat($id)
    {
        $judul = "Lihat Aplikasi";
        $aplikasi = AplikasiModel::find($id);
       return view('content.admin.aplikasi.aplikasi_lihat',compact('judul','aplikasi'));
    }

    public function aplikasi_edit($id)
    {
        $judul = "Edit Aplikasi";
        $aplikasi = AplikasiModel::find($id);
       return view('content.admin.aplikasi.aplikasi_edit',compact('judul','aplikasi'));
    }

    public function aplikasi_update(Request $request)
    {
        $aplikasi = $request->input('aplikasi');
        $id = $request->input('id');
        $id_layanan = $request->input('id_layanan');
        // dd($request->all());
        $aplikasi_new = AplikasiModel::find($id);
        $aplikasi_new->nama_aplikasi = $aplikasi;
        $aplikasi_new->id_layanan = $id_layanan;
        $aplikasi_new->save();

        return redirect()->route('aplikasi',$id_layanan);
    }

    public function aplikasi_hapus($id)
    {
       $hapus = AplikasiModel::find($id);
       $id_layanan = $hapus->id_layanan;
       $hapus->delete();
       return redirect()->route('aplikasi',$id_layanan);
    }
}
