<?php

namespace App\Http\Controllers;

use App\Models\AplikasiModel;
use Illuminate\Http\Request;
use App\Models\TutorialModel;
use App\Models\LayananModel;
class TutorialController extends Controller
{
    public function tutorial ()
    {
        $judul = "Tutorial";
        $tutorial = TutorialModel::all();
        return view ('content.admin.tutorial.tutorial', compact('judul','tutorial'));
    }

    public function tutorial_tambah ()
    {
        $judul = "Formulir Tutorial";
        $layanan = AplikasiModel::all();
        return view ('content.admin.tutorial.tutorial_form', compact('judul','layanan'));
    }

    public function tutorial_simpan(Request $request)
    {
        $tutorial = new TutorialModel;
        $tutorial->id_layanan = $request->input('id_layanan');
        $tutorial->keterangan = $request->input('keterangan');
        if($request->hasFile('file_tutorial')){
            $path  = $request->file('file_tutorial')->storePublicly('public/file_tutorial'.date('Y').'/'.date('m').'/'.date("d"));
            $tutorial->file = $path;
        }
        $tutorial->save();
        return redirect()->route('tutorial');
    }

    public function tutorial_ubah($id)
    {
        $judul = "Edit Tutorial";
        $tutorial = TutorialModel::find($id);
        $layanan = AplikasiModel::all();
        return view('content.admin.tutorial.tutorial_edit',compact('judul','tutorial','layanan'));
    }

    public function tutorial_update(Request $request)
    {

        // dd($request->all());
        $id = $request->input('id_tutorial');
        $tutorial = TutorialModel::find($id);
        $tutorial->id_layanan = $request->input('id_layanan');
        $tutorial->keterangan = $request->input('keterangan');
        if($request->hasFile('file_tutorial')){
            $path  = $request->file('file_tutorial')->storePublicly('public/file_tutorial'.date('Y').'/'.date('m').'/'.date("d"));
            $tutorial->file = $path;
        }
        $tutorial->save();
        return redirect()->route('tutorial');
    }

    public function tutorial_lihat ($id)
    {
        $judul = "Lihat Tutorial";
        $tutorial = TutorialModel::find($id);
        return view ('content.admin.tutorial.tutorial_lihat', compact('judul','tutorial'));
    }

    public function tutorial_hapus($id)
    {
        $hapus = TutorialModel::find($id);
        $hapus->delete();
        return redirect()->route('tutorial');
    }
}
