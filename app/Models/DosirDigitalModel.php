<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DosirDigitalModel extends Model
{
    use HasFactory;
    protected $table = 'dosir_digital';
    protected $guarded = [];
}
