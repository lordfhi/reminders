<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReminderModel extends Model
{
    use HasFactory;

    protected $table = 'data_reminders';
    protected $guarded = [];
    public $timestamps = false;

    public function kategori() {
        return $this->belongsTo('App\Models\AplikasiModel','id_layanan','id');
    }


    public function pegawai() {
        return $this->belongsTo('App\Models\User','id_users','id');
    }
}
