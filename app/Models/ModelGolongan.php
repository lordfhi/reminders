<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ModelGolongan extends Model
{
    use HasFactory;
    protected $table = 'golongan';
    protected $guarded = [];
    public $timestamps = false;
}
