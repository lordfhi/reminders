<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ModelPangkat extends Model
{
    use HasFactory;
    protected $table = 'pangkat';
    protected $guarded = [];
    public $timestamps = false;
}
