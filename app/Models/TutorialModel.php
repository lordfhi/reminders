<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TutorialModel extends Model
{
    use HasFactory;
    protected $table = 'tutorial';
    protected $guarded = [];
    public $timestamps = false;

    public function kategori() {
        return $this->belongsTo('App\Models\AplikasiModel','id_layanan','id');
    }
}
